import 'dart:ui';

import 'package:flutter/material.dart';

const MainHeading = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 30
);

const SubHeading = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 20
);

const BasicHeading = TextStyle(
  fontSize: 15
);

const PriceHeading = TextStyle(
  fontSize: 20
);