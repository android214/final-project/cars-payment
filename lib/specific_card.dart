import 'package:flutter/material.dart';
import 'package:project_term/utils.dart';

class SpecificsCard extends StatelessWidget {
  final String color;
  final String name;

  SpecificsCard({required this.color, required this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      width: 100,
      decoration:BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
          children: [
            Text(name, style: BasicHeading,),
            SizedBox(height: 5,),
            Text(color, style: SubHeading,),
            SizedBox(height: 5),
          ],
      ),
    );
  }
}
