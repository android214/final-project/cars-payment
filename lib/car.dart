
class CarItem {
  final String name;
  final double price;
  final String img;
  final String color;

  CarItem({
    required this.name,
    required this.price,
    required this.img,
    required this.color
  });
}

CarsList allCars = CarsList(cars: [
  CarItem(
      name: 'Honda Civic 2018',
      price: 964000,
      img: 'assets/Civic_2018.png',
      color: 'White'
  ),
  CarItem(
      name: 'Honda Civic 2020',
      price: 1229000,
      img: 'assets/Civic_2019.png',
      color: 'Black'
  ),
  CarItem(
      name: 'Honda Civic 2021',
      price: 964000,
      img: 'assets/Civic_2021.png',
      color: 'White'
  ),
  CarItem(
      name: 'Honda City RS Turbo 2020',
      price: 739000,
      img: 'assets/City_2020.png',
      color: 'Red'
  ),
  CarItem(
      name: 'Honda City 2017',
      price: 550000,
      img: 'assets/City_2017.png',
      color: 'Blue Metalic'
  ),
  CarItem(
      name: 'Toyota Yaris 2020',
      price: 609000,
      img: 'assets/Yaris_2020.png',
      color: 'Citrus Mica Metallic'
  ),
  CarItem(
      name: 'Toyota Corolla Altis 2021',
      price: 879000,
      img: 'assets/Altis_2021.png',
      color: 'Celestite Gray'
  ),
  CarItem(
      name: 'Toyota Corolla Altis 2019',
      price: 799000,
      img: 'assets/Altis_2019.png',
      color: 'Silver Metallic'
  ),
  CarItem(
      name: 'Toyota CAMRY 2019',
      price: 1445000,
      img: 'assets/Camry_2019.png',
      color: 'White'
  ),
  CarItem(
      name: 'Toyota CAMRY 2016',
      price: 1399000,
      img: 'assets/Camry_2016.png',
      color: 'Citrus Mica Metallic '
  ),
  CarItem(
      name: 'Mazda BT-50 2016',
      price: 777000,
      img: 'assets/BT-50_2016.png',
      color: 'Black'
  ),
  CarItem(
      name: 'Mazda CX-3 2018',
      price: 879000,
      img: 'assets/mazda3_cx-3_2018.png',
      color: 'Red Metallic'
  ),
]);

class CarsList {
  List<CarItem> cars;

  CarsList({required this.cars});
}