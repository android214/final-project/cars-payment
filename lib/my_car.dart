import 'package:flutter/material.dart';
import 'package:project_term/car.dart';
import 'package:project_term/utils.dart';
import 'package:project_term/installment_car.dart';


class MyCars extends StatelessWidget {
  const MyCars({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      itemCount: allCars.cars.length,
      itemBuilder: (ctx, i) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: GestureDetector(
          onTap: () {
            Navigator.of(context)
              .push(MaterialPageRoute(builder: (ctx) => InstallmentCar(
                name: allCars.cars[i].name,
                price: allCars.cars[i].price,
                img: allCars.cars[i].img,
                color: allCars.cars[i].color,
              )));
          },
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [BoxShadow(
                color: Colors.black26,
                blurRadius: 5,
                spreadRadius: 1
              )]
            ),
            child: Column(
              children: [
                Hero(
                  tag: allCars.cars[i],
                  child: Image.asset(allCars.cars[i].img)),
                Text(
                  allCars.cars[i].name,
                  style: BasicHeading,
                ),
                Text(
                  (allCars.cars[i].price).toString(),
                  style: SubHeading,
                ),
                Text('THB')
              ],
            ),
          ),
        ),
      ),
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
    );
  }
}