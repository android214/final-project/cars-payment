import 'package:flutter/material.dart';
import 'package:project_term/utils.dart';
import 'package:project_term/specific_card.dart';

class InstallmentCar extends StatelessWidget {
  final String name;
  final double price;
  final String img;
  final String color;

InstallmentCar({
  required this.name,
  required this.price,
  required this.img,
  required this.color,
});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[50],
        actions: [
          IconButton(
            onPressed: null,
            icon: Icon(Icons.bookmark,size: 30,color: Colors.grey,)
          )
        ]
      ),
      body: ListView(
        children: [
          Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text(name, style: MainHeading),
              Hero(tag:name, child: Image.asset(img)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SpecificsCard(
                    name: 'Color',
                    color: color,
                  ),
                ],
              ),
              SizedBox(height: 20),
              Text('INSTALLMENT',
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20
                ),
              ),
              SizedBox(height: 30),
              Text((price).toString(), style: PriceHeading),
              SizedBox(height: 10),
              Text('เงินดาวน์: 50000', style: PriceHeading),
              SizedBox(height: 10),
              Text('จำนวนงวด: 96งวด (8ปี)', style: PriceHeading),
              SizedBox(height: 10),
              Text('ดอกเบี้ย: 1.5', style: PriceHeading),
              SizedBox(height: 10),
              Text('ค่างวด/เดือน: xxxxx', style: PriceHeading),
              SizedBox(height: 10),
              Text('ชำระแล้ว: xxxxxxx', style: PriceHeading),
              SizedBox(height: 10),
              Text('ยอดจัดที่ยังต้องชำระ: xxxxxxxx', style: PriceHeading),
              SizedBox(height: 10),
            ],
          ),
        ),
      ]
      ),
   );
  }
}