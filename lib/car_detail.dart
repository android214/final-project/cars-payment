import 'package:flutter/material.dart';
import 'package:project_term/utils.dart';
import 'package:project_term/specific_card.dart';

class CarDetail extends StatelessWidget {
  final String name;
  final double price;
  final String img;
  final String color;

CarDetail({
  required this.name,
  required this.price,
  required this.img,
  required this.color,
});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[50],
        actions: [
          IconButton(
            onPressed: null,
            icon: Icon(Icons.bookmark,size: 30,color: Colors.grey,)
          )
        ]
      ),
      body: ListView(
        children: [
          Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Text(name, style: MainHeading),
              Hero(tag:name, child: Image.asset(img)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SpecificsCard(
                    name: 'Color',
                    color: color,
                  ),
                ],
              ),
              SizedBox(height: 20),
              Text('INSTALLMENT',
                style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20
                ),
              ),
              SizedBox(height: 30),
              Text((price).toString(), style: BasicHeading),
              TextFormField(
                validator: (value) {
                  var num = int.tryParse(value!);
                  if(num == null || num <= 0){
                    return 'กรุณาใส่เงินดาวน์';
                  }
                  return null;
                },
                decoration: InputDecoration(labelText: 'เงินดาวน์'),
                keyboardType: TextInputType.number,
              ),
              DropdownButtonFormField(
                  items: [
                    DropdownMenuItem(child: Text('-'), value: '-'),
                    DropdownMenuItem(child: Text('12งวด (1ปี)'), value: '12'),
                    DropdownMenuItem(child: Text('24งวด (2ปี)'), value: '24'),
                    DropdownMenuItem(child: Text('36งวด (3ปี)'), value: '36'),
                    DropdownMenuItem(child: Text('48งวด (4ปี)'), value: '48'),
                    DropdownMenuItem(child: Text('60งวด (5ปี)'), value: '60'),
                    DropdownMenuItem(child: Text('72งวด (6ปี)'), value: '72'),
                    DropdownMenuItem(child: Text('84งวด (7ปี)'), value: '84'),
                    DropdownMenuItem(child: Text('96งวด (8ปี)'), value: '96'),
                    DropdownMenuItem(child: Text('108งวด (9ปี)'), value: '108'),
                  ],
                  onChanged: (String? newValue) {
                  },
                  decoration: InputDecoration(labelText: 'จำนวนงวด'),
                ),
              TextFormField(
                validator: (value) {
                  var num = int.tryParse(value!);
                  if(num == null || num <= 0){
                    return 'กรุณาใส่ดอกเบี้ย';
                  }
                  return null;
                },
                decoration: InputDecoration(labelText: 'ดอกเบี้ย %'),
                keyboardType: TextInputType.number,
              ),
              SizedBox(height: 10),
              ElevatedButton(
                onPressed: null,
                child: Text(
                  'Calculate',
                  style: TextStyle(fontSize: 25, color: Colors.blueGrey),
                ),
              )
            ],
          ),
        ),
      ]
      ),
   );
  }
}